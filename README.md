# Oh my game

The aim of the Oh My Game project is to develop a web application that will enable board game players to connect with other players near to them and share the games they own.

Le Projet Oh My Game à pour objectif de développer une application web permettant aux joueurs de jeux de société d’entrer en relation avec d’autres joueurs à proximité de leur localisation afin de partager les jeux en possession de chacun. 


## Récupérer le projet

```
git clone git@github.com:O-clock-Bao/projet-07-oh-my-game.git
```

## Initialisation

```
composer install
```

.env.local : Définir sa base de données

## Développement

Se positionner sur la branche main,
Tirer la branche main, récuperer le travail commun,
Se déplacer sur la branche de travail dev_ton_nom,
Merger le travail commun sur la branche de travail,

```
git switch main
git pull
git switch 'nom_de_branche'
git merge main
```

Tu as récupéré le projet et tu es prêt à développer en sécurité

## Serveur local avec Symfony CLI
```
symfony serve -d
```

## Prêt à révolutionner le monde ?

```
git switch -c 'feature_name'
```
Ici tu peux faire des folies

## Une fois ton larcin effectué

> Récupérer le main pour résoudre les conflits de canard,
```
git switch main
git pull
git switch 'ta_branche'
git merge main
```
Ici vsCode te propose de résoudre ce qui pose problème 
:P Bon appétit ! 

> merge la feature sur ta branche dev

Verifier les fichiers à uploder,
Ajouter les fichiers,
Créer le commit,

```
git status
git add .
git commit -m 'feature - comment'
git switch 'ta_branche'
git merge 'feature_name'
```

## Fini de bosser ?

Envoyer son travail sur github
```
git push
```

Créer une Pull Request sur Github


## Modification de dernière minute

```
git add .
git commit --ammend --no-edit
```

## Tout est ok ??
L'apéro t'attends !
